
profiles
========

Simple zsh, bash, tmux and vim profiles.

To use this, simply run the following lines from your command line.

```bash
export PROFILES=$HOME/.profiles

#
# Clone this repository
#

git clone git@bitbucket.org:ccdm-curtin/profiles.git $PROFILES
# if you have a bitbucket account with ssh key-pairs, otherwise:
git clone https://darcyabjones@bitbucket.org/ccdm-curtin/profiles.git $PROFILES

#
# Link the profile files to the home dir
#
ln -sf $PROFILES/aliases $HOME/.aliases
ln -sf $PROFILES/bash_profile $HOME/.bash_profile
ln -sf $PROFILES/bashrc $HOME/.bashrc
ln -sf $PROFILES/env $HOME/.env
ln -sf $PROFILES/Rprofile $HOME/.Rprofile
ln -sf $PROFILES/tmux.conf $HOME/.tmux.conf
ln -sf $PROFILES/zshrc $HOME/.zshrc

git clone https://github.com/zsh-users/antigen.git $HOME/.antigen
```
If you don't have a bitbucket account go to <https://bitbucket.org/ccdm-curtin/profiles/overview> and download the thing manually.

If you're running bash (default for most distros) switch to zsh.

```bash
sudo apt install zsh

sudo chsh $(which zsh) <username>
```

Obviously replacing `<username>` with your actual username.
Then it's just a matter of starting a new session or rebooting the computer.
